<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'symbol', 'code', 'currency_type_id'];

    public function accounts()
    {
        return $this->belongsTo(Account::class, 'id', 'currency_id');
    }

    public function type() {
        return $this->hasOne(CurrencyType::class, 'id', 'currency_type_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperationCategory extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'slug', 'operation_type_id'];

    public function type()
    {
        return $this->hasOne(OperationType::class, 'id', 'operation_type_id');
    }
}

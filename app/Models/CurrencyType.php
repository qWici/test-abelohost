<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencyType extends Model
{
    use HasFactory;

    protected $fillable = ['title'];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'id', 'currency_type_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $fillable = ['owner_id', 'account_type_id', 'currency_id'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }

    public function type()
    {
        return $this->hasOne(AccountType::class, 'id', 'account_type_id');
    }

    public function currency()
    {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    public function operationDays()
    {
        return $this->hasMany(OperationDay::class, 'account_id');
    }
}

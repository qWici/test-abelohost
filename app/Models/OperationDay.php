<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperationDay extends Model
{
    use HasFactory;

    protected $fillable = ['account_id'];

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    public function operations()
    {
        return $this->hasMany(Operation::class, 'operation_day_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    use HasFactory;

    protected $fillable = ['operation_day_id', 'operation_type_id', 'operation_category_id', 'sum', 'comment'];

    public function operationDay()
    {
        return $this->belongsTo(OperationDay::class, 'operation_day_id', 'id');
    }

    public function type()
    {
        return $this->hasOne(OperationType::class, 'id', 'operation_type_id');
    }

    public function category()
    {
        return $this->hasOne(OperationCategory::class, 'id', 'operation_category_id');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\OperationDay;
use Illuminate\Http\Request;

class OperationDayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OperationDay  $operationDay
     * @return \Illuminate\Http\Response
     */
    public function show(OperationDay $operationDay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OperationDay  $operationDay
     * @return \Illuminate\Http\Response
     */
    public function edit(OperationDay $operationDay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OperationDay  $operationDay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OperationDay $operationDay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OperationDay  $operationDay
     * @return \Illuminate\Http\Response
     */
    public function destroy(OperationDay $operationDay)
    {
        //
    }
}

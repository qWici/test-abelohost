<?php

namespace Database\Seeders;

use App\Models\OperationType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\User::factory(1)->create();
         $this->call(AccountTypeSeeder::class);
         $this->call(CurrencyTypeSeeder::class);
         $this->call(CurrencySeeder::class);
         $this->call(AccountSeeder::class);
         $this->call(OperationDaySeeder::class);
         $this->call(OperationTypeSeeder::class);
         $this->call(OperationCategorySeeder::class);
         $this->call(OperationSeeder::class);
    }
}

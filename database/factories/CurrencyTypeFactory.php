<?php

namespace Database\Factories;

use App\Models\CurrencyType;
use Illuminate\Database\Eloquent\Factories\Factory;

class CurrencyTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CurrencyType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => 'Cash'
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\OperationCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class OperationCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OperationCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => 'Meal',
            'slug' => 'meal',
            'operation_type_id' => 1
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Operation;
use Illuminate\Database\Eloquent\Factories\Factory;

class OperationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Operation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'operation_day_id' => 1,
            'sum' => 100.78,
            'comment' => 'Pizza',
            'operation_type_id' => 1,
            'operation_category_id' => 1
        ];
    }
}

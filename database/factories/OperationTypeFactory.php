<?php

namespace Database\Factories;

use App\Models\OperationType;
use Illuminate\Database\Eloquent\Factories\Factory;

class OperationTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OperationType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => 'Income',
            'type' => 'income'
        ];
    }
}
